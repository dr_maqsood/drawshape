﻿using DrawShap.Service;
using Xunit;

namespace DrawShape.UnitTest
{    
    public class TestShapeDescription
    {
        [Fact]
        public void GetCircleWith100Radius()
        {
            var shapeDescription = "Draw a circle with a radius of 100";
            var shapeProcessor = new ShapeDescriptionProcessor(shapeDescription);
            var shapeDetail = shapeProcessor.GetShapeDetail();            

            Assert.Equal(ShapeType.Circle, shapeDetail.GetShapeType());
            Assert.Equal(100, shapeDetail.GetValue1());
        }

        [Fact]
        public void GetSquareWith200Length()
        {
            var shapeDescription = "Draw a square with a side length of 200";
            var shapeProcessor = new ShapeDescriptionProcessor(shapeDescription);
            var shapeDetail = shapeProcessor.GetShapeDetail();

            Assert.Equal(ShapeType.Square, shapeDetail.GetShapeType());
            Assert.Equal(200, shapeDetail.GetValue1());
        }
        [Fact]
        public void GetRectannleWith250Width400Height()
        {
            var shapeDescription = "Draw a rectangle with a width of 250 and a height of 400";
            var shapeProcessor = new ShapeDescriptionProcessor(shapeDescription);
            var shapeDetail = shapeProcessor.GetShapeDetail();

            Assert.Equal(ShapeType.Rectangle, shapeDetail.GetShapeType());
            Assert.Equal(250, shapeDetail.GetValue1());
            Assert.Equal(400, shapeDetail.GetValue2());
        }
        [Fact]
        public void GetOctagonWith200Length()
        {
            var shapeDescription = "Draw an octagon with a side length of 200";
            var shapeProcessor = new ShapeDescriptionProcessor(shapeDescription);
            var shapeDetail = shapeProcessor.GetShapeDetail();

            Assert.Equal(ShapeType.Octagon, shapeDetail.GetShapeType());
            Assert.Equal(200, shapeDetail.GetValue1());
        }
        [Fact]
        public void GetHexagonWith200Length()
        {
            var shapeDescription = "Draw a hexagon with a side length of 200";
            var shapeProcessor = new ShapeDescriptionProcessor(shapeDescription);
            var shapeDetail = shapeProcessor.GetShapeDetail();

            Assert.Equal(ShapeType.Hexagon, shapeDetail.GetShapeType());
            Assert.Equal(200, shapeDetail.GetValue1());
        }
        [Fact]
        public void GetIsoscelesTriangleWith250Width400Height()
        {
            var shapeDescription = "Draw an isosceles triangle with a height of 200 and a width of 100";
            var shapeProcessor = new ShapeDescriptionProcessor(shapeDescription);
            var shapeDetail = shapeProcessor.GetShapeDetail();

            Assert.Equal(ShapeType.IsoscelesTriangle, shapeDetail.GetShapeType());
            Assert.Equal(100, shapeDetail.GetValue1());
            Assert.Equal(200, shapeDetail.GetValue2());
        }
        [Fact]
        public void GetEquilateralTriangleWith200Length()
        {
            var shapeDescription = "Draw an equilateral triangle with a length of 200";
            var shapeProcessor = new ShapeDescriptionProcessor(shapeDescription);
            var shapeDetail = shapeProcessor.GetShapeDetail();

            Assert.Equal(ShapeType.EquilateralTriangle, shapeDetail.GetShapeType());
            Assert.Equal(200, shapeDetail.GetValue1());
        }
        [Fact]
        public void GetParallelogramWith250Width400Height()
        {
            var shapeDescription = "Draw an parallelogram with a width of 200 and a height of 100";
            var shapeProcessor = new ShapeDescriptionProcessor(shapeDescription);
            var shapeDetail = shapeProcessor.GetShapeDetail();

            Assert.Equal(ShapeType.Parallelogram, shapeDetail.GetShapeType());
            Assert.Equal(200, shapeDetail.GetValue1());
            Assert.Equal(100, shapeDetail.GetValue2());
        }
    }
}