﻿using DrawShap.Service;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace DrawShape.UI
{
    public partial class FDrawShape : Form
    {
        public FDrawShape()
        {
            InitializeComponent();
        }

        private void btnDraw_Click(object sender, EventArgs e)
        {
            ShapeDescriptionProcessor shapeProcessor = new ShapeDescriptionProcessor(txtShapeDescription.Text);
            var shape = shapeProcessor.GetShapeDetail();
            var shapeType = shape.GetShapeType();
            lblError.Text = "";
            if (shapeType != ShapeType.Undefined)
            {
                Graphics myGraphics = grpShape.CreateGraphics();
                Pen myPen = new Pen(Color.Blue);
                grpShape.Refresh();

                switch (shapeType)
                {
                    case ShapeType.Circle:
                        #region Draw Circle
                        var x = grpShape.Width / 2;
                        var y = grpShape.Height / 2;
                        int radius = shape.GetValue1();
                        myGraphics.DrawEllipse(myPen, x, y, radius, radius);
                        #endregion
                        break;
                    case ShapeType.Rectangle:
                        #region Draw Rectangle
                        int width = shape.GetValue1();
                        int height = shape.GetValue2().Value;
                        x = grpShape.Width / 2;
                        y = grpShape.Height / 2;
                        myGraphics.DrawRectangle(myPen, x, y, width, height);
                        #endregion
                        break;
                    case ShapeType.Square:
                        #region Draw Square
                        var length = shape.GetValue1();
                        x = grpShape.Width / 2;
                        y = grpShape.Height / 2;
                        myGraphics.DrawRectangle(myPen, x, y, length, length);
                        #endregion
                        break;
                    case ShapeType.Octagon:
                        #region Draw Octagon
                        length = shape.GetValue1();
                        //Get the middle of the panel
                        x = grpShape.Width / 2;
                        y = grpShape.Height / 2;

                        var R = length; //px radius 
                        int R2 = Convert.ToInt32(R / Math.Sqrt(2));

                        var pt = new Point[8];
                        pt[0] = new Point(x, y - R);
                        pt[1] = new Point(x + R2, y - R2);
                        pt[2] = new Point(x + R, y);
                        pt[3] = new Point(x + R2, y + R2);
                        pt[4] = new Point(x, y + R);
                        pt[5] = new Point(x - R2, y + R2);
                        pt[6] = new Point(x - R, y);
                        pt[7] = new Point(x - R2, y - R2);

                        myGraphics.DrawPolygon(myPen, pt);
                        #endregion
                        break;
                    case ShapeType.IsoscelesTriangle:
                        #region Draw Isosceles Triangle
                        myGraphics = grpShape.CreateGraphics();
                        width = shape.GetValue1();
                        //Get the middle of the panel
                        x = grpShape.Width / 2;
                        y = grpShape.Height / 2;

                        float angle = 0;                        

                        pt = new Point[3];
                        pt[0] = new Point(x, y);
                        pt[1] = new Point(Convert.ToInt32(x + width * Math.Cos(angle)), Convert.ToInt32((y + width * Math.Sin(angle))));
                        pt[2] = new Point(Convert.ToInt32((x + width * Math.Cos(angle + Math.PI / 3))), Convert.ToInt32((y + width * Math.Cos(angle + Math.PI / 3))));

                        myGraphics.DrawPolygon(myPen, pt);
                        #endregion
                        break;
                    case ShapeType.Hexagon:
                        #region Draw Hexagon
                        width = shape.GetValue1();
                        x = grpShape.Width / 2;
                        y = grpShape.Height / 2;

                        var pointF = new PointF[6];
                        var r = width; //70 px radius 

                        //Create 6 points
                        for (int a = 0; a < 6; a++)
                        {
                            pointF[a] = new PointF(x + r * (float)Math.Cos(a * 60 * Math.PI / 180f), y + r * (float)Math.Sin(a * 60 * Math.PI / 180f));
                        }
                        myGraphics.DrawPolygon(myPen, pointF);
                        #endregion
                        break;
                    case ShapeType.Pentagon:
                        #region Draw Pentagon
                        width = shape.GetValue1();
                        //Get the middle of the panel
                        x = grpShape.Width / 2;
                        y = grpShape.Height / 2;

                        R = width; //px radius                        

                        pointF = new PointF[5];
                        double theta = -Math.PI / 2.0;
                        double dtheta = 2.0 * Math.PI / 5.0;

                        //Create 5 points
                        for (int i = 0; i < 5; i++)
                        {
                            pointF[i] = new PointF(x + (float)(R * Math.Cos(theta)), y + (float)(R * Math.Sin(theta)));
                            theta += dtheta;
                        }

                        myGraphics.DrawPolygon(myPen, pointF);
                        #endregion
                        break;
                    case ShapeType.EquilateralTriangle:
                        #region Draw Equilateral Triangle
                        width = shape.GetValue1();
                        x = grpShape.Width / 2;
                        y = grpShape.Height / 2;

                        pointF = new PointF[3];
                        pointF[0] = new PointF(x, y);
                        pointF[1] = new PointF(x + width / 2, y + width);
                        pointF[2] = new PointF(x - width / 2, y + width);

                        myGraphics.DrawPolygon(myPen, pointF);
                        #endregion
                        break;
                    case ShapeType.Parallelogram:
                        #region Draw Parallelogram
                        width = shape.GetValue1();
                        height = shape.GetValue2().Value;
                        var lean = 40;
                        x = grpShape.Width / 2;
                        y = grpShape.Height / 2;
                        var points = new Point[4];
                        points[0] = new Point(x, y);
                        points[1] = new Point(x + lean, y + height);
                        points[2] = new Point(x + lean + width, y + height);
                        points[3] = new Point(x + width, y);

                        myGraphics.DrawPolygon(myPen, points);
                        #endregion
                        break;
                    case ShapeType.Oval:
                        #region Draw Oval
                        width = shape.GetValue1();
                        height = shape.GetValue2().Value;
                        x = grpShape.Width / 2;
                        y = grpShape.Height / 2;
                        myGraphics.DrawEllipse(myPen, x, y, width, height);
                        #endregion
                        break;                        
                    case ShapeType.ScaleneTriangle:
                        //TODO: Add ScaleneTriangle logic here
                        break;
                    case ShapeType.Heptagon:
                        //TODO: Add Heptagon logic here
                        break;
                }
            }
            else
                lblError.Text = "Shape not supported";
        }

        private void lstExamples_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtShapeDescription.Text = lstExamples.SelectedItem.ToString();
        }
    }
}
