﻿namespace DrawShape.UI
{
    partial class FDrawShape
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtShapeDescription = new System.Windows.Forms.TextBox();
            this.btnDraw = new System.Windows.Forms.Button();
            this.grpShape = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.lstExamples = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtShapeDescription
            // 
            this.txtShapeDescription.Location = new System.Drawing.Point(12, 23);
            this.txtShapeDescription.Multiline = true;
            this.txtShapeDescription.Name = "txtShapeDescription";
            this.txtShapeDescription.Size = new System.Drawing.Size(342, 47);
            this.txtShapeDescription.TabIndex = 0;
            // 
            // btnDraw
            // 
            this.btnDraw.Location = new System.Drawing.Point(133, 93);
            this.btnDraw.Name = "btnDraw";
            this.btnDraw.Size = new System.Drawing.Size(75, 23);
            this.btnDraw.TabIndex = 1;
            this.btnDraw.Text = "Draw";
            this.btnDraw.UseVisualStyleBackColor = true;
            this.btnDraw.Click += new System.EventHandler(this.btnDraw_Click);
            // 
            // grpShape
            // 
            this.grpShape.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpShape.Location = new System.Drawing.Point(34, 128);
            this.grpShape.Name = "grpShape";
            this.grpShape.Size = new System.Drawing.Size(816, 473);
            this.grpShape.TabIndex = 2;
            this.grpShape.TabStop = false;
            this.grpShape.Text = "Shape";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(244, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Enter the shape description you would like to draw";
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(13, 73);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 13);
            this.lblError.TabIndex = 4;
            // 
            // lstExamples
            // 
            this.lstExamples.FormattingEnabled = true;
            this.lstExamples.Items.AddRange(new object[] {
            "Draw a circle with a radius of 100",
            "Draw a square with a side length of 200",
            "Draw a rectangle with a width of 250 and a height of 400",
            "Draw an octagon with a side length of 200",
            "Draw an isosceles triangle with a height of 200 and a width of 100",
            "Draw a hexagon with a side length of 200",
            "Draw an equilateral triangle with a length of 200"});
            this.lstExamples.Location = new System.Drawing.Point(434, 23);
            this.lstExamples.Name = "lstExamples";
            this.lstExamples.Size = new System.Drawing.Size(416, 95);
            this.lstExamples.TabIndex = 5;
            this.lstExamples.SelectedIndexChanged += new System.EventHandler(this.lstExamples_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(431, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Examples of descriptions";
            // 
            // FDrawShape
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 613);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lstExamples);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.grpShape);
            this.Controls.Add(this.btnDraw);
            this.Controls.Add(this.txtShapeDescription);
            this.Name = "FDrawShape";
            this.Text = "Draw Shape";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtShapeDescription;
        private System.Windows.Forms.Button btnDraw;
        private System.Windows.Forms.GroupBox grpShape;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.ListBox lstExamples;
        private System.Windows.Forms.Label label2;
    }
}

