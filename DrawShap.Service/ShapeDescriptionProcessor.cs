﻿using System;

namespace DrawShap.Service
{
    public class ShapeDescriptionProcessor
    {
        private string _shapeDescription;
        /// <summary>
        /// Shape description processor
        /// </summary>
        /// <param name="shapeDescription">semi-natural language description</param>
        public ShapeDescriptionProcessor(string shapeDescription)
        {
            _shapeDescription = shapeDescription;
        }
        /// <summary>
        /// Get shape detail
        /// </summary>
        /// <returns>ShapeDetail</returns>
        public ShapeDetail GetShapeDetail()
        {            
            var shapeDetail = new ShapeDetail(0, 0, ShapeType.Undefined);
            try
            {
                //Draw a(n) < shape > with a(n) < measurement > of<amount>(and a(n) < measurement > of<amount>)
                //Draw a circle with a radius of 100
                //Draw a square with a side length of 200
                //Draw a rectangle with a width of 250 and a height of 400
                //Draw an octagon with a side length of 200
                //Draw an isosceles triangle with a height of 200 and a width of 100
                //Draw a hexagon with a side length of 200
                //Draw an equilateral triangle with a length of 200

                if (!string.IsNullOrEmpty(_shapeDescription))
                {
                    var descriptionArray =_shapeDescription.ToLower().Trim().Split(' ');
                    //shape type is at the 2nd index
                    switch(descriptionArray[2])
                    {
                        case "circle":
                            //radius is at the 7th index
                            int radius = Convert.ToInt32(descriptionArray[7]);
                            shapeDetail = new ShapeDetail(radius, null, ShapeType.Circle);
                            break;
                        case "square":
                            //length is at the 7th index
                            int length = Convert.ToInt32(descriptionArray[8]);
                            shapeDetail = new ShapeDetail(length, null, ShapeType.Square);
                            break;
                        case "rectangle":                            
                            var widthHeightProcessor = new WidthHeightProcessor(_shapeDescription);
                            //get width and height from description
                            var widthHeight = widthHeightProcessor.GetWidthHeight(5, 10);
                            shapeDetail = new ShapeDetail(widthHeight.Item1, widthHeight.Item2, ShapeType.Rectangle);
                            break;
                        case "octagon":
                            //length is at the 8th index
                            length = Convert.ToInt32(descriptionArray[8]);
                            shapeDetail = new ShapeDetail(length, null, ShapeType.Octagon);
                            break;
                        case "isosceles":
                            //get width and height from description
                            widthHeightProcessor = new WidthHeightProcessor(_shapeDescription);
                            widthHeight = widthHeightProcessor.GetWidthHeight(6, 11);
                            shapeDetail = new ShapeDetail(widthHeight.Item1, widthHeight.Item2, ShapeType.IsoscelesTriangle);
                            break;
                        case "hexagon":
                            //length is at the 8th index
                            length = Convert.ToInt32(descriptionArray[8]);
                            shapeDetail = new ShapeDetail(length, null, ShapeType.Hexagon);
                            break;
                        case "heptagon":
                            //length is at the 8th index
                            length = Convert.ToInt32(descriptionArray[8]);
                            shapeDetail = new ShapeDetail(length, null, ShapeType.Heptagon);
                            break;
                        case "pentagon":
                            //length is at the 8th index
                            length = Convert.ToInt32(descriptionArray[8]);
                            shapeDetail = new ShapeDetail(length, null, ShapeType.Pentagon);
                            break;
                        case "scalene":
                            //length is at the 8th index
                            length = Convert.ToInt32(descriptionArray[8]);
                            shapeDetail = new ShapeDetail(length, null, ShapeType.ScaleneTriangle);
                            break;
                        case "parallelogram":
                            //get width and height from description
                            widthHeightProcessor = new WidthHeightProcessor(_shapeDescription);
                            widthHeight = widthHeightProcessor.GetWidthHeight(5, 10);
                            shapeDetail = new ShapeDetail(widthHeight.Item1, widthHeight.Item2, ShapeType.Parallelogram);
                            break;
                        case "equilateral":
                            //length is at the 8th index
                            length = Convert.ToInt32(descriptionArray[8]);
                            shapeDetail = new ShapeDetail(length, null, ShapeType.EquilateralTriangle);
                            break;
                        case "oval":
                            //get width and height from description
                            widthHeightProcessor = new WidthHeightProcessor(_shapeDescription);
                            widthHeight = widthHeightProcessor.GetWidthHeight(5, 10);
                            shapeDetail = new ShapeDetail(widthHeight.Item1, widthHeight.Item2, ShapeType.Oval);
                            break;

                        default:
                            shapeDetail = new ShapeDetail(0, 0, ShapeType.Undefined);
                            break;
                    }
                }
            }
            catch(Exception ex)
            {
                //TODO: Log the error
            }
            return shapeDetail;
        }        
    }
}
