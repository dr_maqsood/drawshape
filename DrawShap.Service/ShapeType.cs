﻿namespace DrawShap.Service
{
    /// <summary>
    /// Supported shape type
    /// </summary>
    public enum ShapeType
    {
        IsoscelesTriangle,
        Square,
        ScaleneTriangle,
        Parallelogram,
        EquilateralTriangle,
        Pentagon,
        Rectangle,
        Hexagon,
        Heptagon,
        Octagon,
        Circle,
        Oval,
        Undefined,       
    }
}