﻿namespace DrawShap.Service
{
    public class ShapeDetail
    {
        private int _value1;
        private int? _value2;
        private ShapeType _shapeType;
        /// <summary>
        /// Shape detail
        /// </summary>
        /// <param name="value1">radius, width, lenght depending on the shape type</param>
        /// <param name="value2">height depending on the shape type</param>
        /// <param name="shapeType">shape type</param>
        public ShapeDetail(int value1, int? value2, ShapeType shapeType)
        {
            _value1 = value1;
            _value2 = value2;
            _shapeType = shapeType;
        }
        /// <summary>
        /// Get value 1 (radius or length or width depending on the shape type)
        /// </summary>
        /// <returns></returns>
        public int GetValue1()
        {
            return _value1;
        }
        /// <summary>
        /// Get value 2 (length depending on the shape type)
        /// </summary>
        /// <returns></returns>
        public int? GetValue2()
        {
            return _value2;
        }
        /// <summary>
        /// Get Shape type
        /// </summary>
        /// <returns></returns>
        public ShapeType GetShapeType()
        {
            return _shapeType;
        }
    }
}
