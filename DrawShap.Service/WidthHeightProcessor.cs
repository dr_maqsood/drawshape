﻿using System;

namespace DrawShap.Service
{
    public class WidthHeightProcessor
    {
        private string _description;

        public WidthHeightProcessor(string descripton)
        {
            _description = descripton;
        }
        /// <summary>
        /// Get width and height from the description
        /// </summary>
        /// <param name="x">word "widht" location</param>
        /// <param name="y">word "height" location</param>
        /// <returns></returns>
        public Tuple<int, int> GetWidthHeight(int x, int y)
        {
            var descriptionArray = _description.ToLower().Split(' ');
            int widthIndex = -1;
            int heightIndex = -1;
            if (descriptionArray[x] == "width")
                widthIndex = x+2;
            else
                if (descriptionArray[x] == "height")
                    heightIndex = x+2;

            if (descriptionArray[y] == "width")
                widthIndex = y+2;
            else
                if (descriptionArray[y] == "height")
                    heightIndex = y+2;

            int width = Convert.ToInt32(descriptionArray[widthIndex]);
            int height = Convert.ToInt32(descriptionArray[heightIndex]);
            
            return Tuple.Create(width, height);
        }
    }
}
